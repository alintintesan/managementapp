package com.alin.managementappejb.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.alin.managementappejb.dto.IdentityDTO;
import com.alin.managementappejb.dto.OrganizationDTO;
import com.alin.managementappejb.exception.CustomException;
import com.alin.managementappejb.model.Identity;
import com.alin.managementappejb.model.Organization;
import com.alin.managementappejb.util.DTOToEntity;
import com.alin.managementappejb.util.EntityToDTO;

@Stateless
@LocalBean
public class IdentityDAO implements IdentityDAORemote {

	@PersistenceContext
	private EntityManager entityManager;
	
	private EntityToDTO entityToDTO = new EntityToDTO();
	private DTOToEntity dtoToEntity = new DTOToEntity();
	
	@Override
	public IdentityDTO findById(int id) {
		Identity identity = entityManager.find(Identity.class, id);
		IdentityDTO identityDTO = entityToDTO.convertIdentity(identity);
		
		return identityDTO;
	}

	@Override
	public List<IdentityDTO> findAll() {
		Query query = entityManager.createQuery("SELECT i FROM Identity i");
				
		List<Identity> identities = query.getResultList();
		System.out.println(identities.toString());
		List<IdentityDTO> dtoIdentities = new ArrayList<>();
		
		for (Identity identity : identities) {
			dtoIdentities.add(entityToDTO.convertIdentity(identity));
		}
		
		return dtoIdentities;
	}

	@Override
	public IdentityDTO create(IdentityDTO identityDTO) {
		Identity identity = dtoToEntity.convertIdentity(identityDTO);
		entityManager.persist(identity);
		entityManager.flush();
		identityDTO.setId(identity.getId());
		
		return identityDTO;
	}

	@Override
	public IdentityDTO update(IdentityDTO identityDTO) {
		Identity identity = dtoToEntity.convertIdentity(identityDTO);
		identity.setId(identityDTO.getId());
		identity = entityManager.merge(identity);
		
		return identityDTO;
	}

	@Override
	public void delete(int id) {
		Identity identity = entityManager.find(Identity.class, id);
		entityManager.remove(identity);
	}

	@Override
	public boolean registerIdentity(IdentityDTO identityDTO) throws CustomException {
		Identity identity = dtoToEntity.convertIdentity(identityDTO);
		entityManager.persist(identity);
		entityManager.flush();
		
		return true;
	}

	@Override
	public IdentityDTO addIdentity(IdentityDTO identityDTO) throws CustomException {
		Identity identity = new Identity(identityDTO.getFirstName(), identityDTO.getLastName(), 1);
		entityManager.persist(identity);
		
		return identityDTO;
	}
}

package com.alin.managementappejb.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.alin.managementappejb.dto.OrganizationDTO;
import com.alin.managementappejb.exception.CustomException;
import com.alin.managementappejb.model.Organization;
import com.alin.managementappejb.util.DTOToEntity;
import com.alin.managementappejb.util.EntityToDTO;

@Stateless
@LocalBean
public class OrganizationDAO implements OrganizationDAORemote {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	private EntityToDTO entityToDTO = new EntityToDTO();
	private DTOToEntity dtoToEntity = new DTOToEntity();
	
	@Override
	public OrganizationDTO findById(int id) {
		Organization org = entityManager.find(Organization.class, id);
		OrganizationDTO orgDTO = entityToDTO.convertOrganization(org);
		
		return orgDTO;
	}
	
	@Override
	public List<OrganizationDTO> findAll() {
		Query query = entityManager.createQuery("SELECT o FROM Organization o");
		@SuppressWarnings("unchecked")
		List<Organization> organizations = query.getResultList();
		
		System.out.println(organizations.toString());
		List<OrganizationDTO> orgsDto = new ArrayList<>();
		for (Organization o : organizations) {
			orgsDto.add(entityToDTO.convertOrganization(o));
		}
		return orgsDto;
	}
	
	@Override
	public OrganizationDTO create(OrganizationDTO entity) {
		Organization org = dtoToEntity.convertOrganization(entity);
		org.setId(entity.getId());
		org = entityManager.merge(org);
		
		return entity;
	}
	
	@Override
	public OrganizationDTO update(OrganizationDTO entity) {
		Organization org = dtoToEntity.convertOrganization(entity);
		entityManager.persist(org);
		entityManager.flush();
		entity.setId(org.getId());
		
		return entity;
	}
	
	@Override
	public void delete(int id) {
		Organization org = entityManager.find(Organization.class, id);
		entityManager.remove(org);
		
	}
	
	@Override
	public OrganizationDTO addOrganization(OrganizationDTO organizationDTO) throws CustomException {
		Organization org = new Organization(organizationDTO.getOrganizationName(), organizationDTO.getCui());
		entityManager.persist(org);
		
		return organizationDTO;
	}
	
	@Override
	public List<OrganizationDTO> getOrganizations() throws CustomException {
		List<Organization> orgs = entityManager.createNamedQuery("findAll", Organization.class).getResultList();
		
		List<OrganizationDTO> dtoOrgs = new ArrayList<OrganizationDTO>();
		
		for (Organization o : orgs) {
			dtoOrgs.add(entityToDTO.convertOrganization(o));
		}
		
		return dtoOrgs;
	}
}

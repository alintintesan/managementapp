package com.alin.managementappejb.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.alin.managementappejb.dto.ChangePasswordDTO;
import com.alin.managementappejb.dto.LoginDTO;
import com.alin.managementappejb.dto.RegisterDTO;
import com.alin.managementappejb.dto.UserDTO;
import com.alin.managementappejb.exception.CustomException;
import com.alin.managementappejb.model.User;
import com.alin.managementappejb.util.DTOToEntity;
import com.alin.managementappejb.util.EntityToDTO;

@Stateless
@LocalBean
public class UserDAO implements UserDAORemote {

	@PersistenceContext
	private EntityManager entityManager;

	public UserDAO() {

	}

	private EntityToDTO entityToDTO = new EntityToDTO();

	private DTOToEntity dtoToEntity = new DTOToEntity();

	@Override
	public UserDTO findById(int id) {
		User user = entityManager.find(User.class, id);
		UserDTO userDTO = entityToDTO.convertUser(user);
		return userDTO;
	}

	@Override
	public List<UserDTO> findAll() {
		Query query = entityManager.createQuery("SELECT u FROM User u");
		@SuppressWarnings("unchecked")
		List<User> users = query.getResultList();
		System.out.println(users.toString());
		List<UserDTO> dtoUsers = new ArrayList<>();
		for (User user : users) {
			dtoUsers.add(entityToDTO.convertUser(user));
		}
		return dtoUsers;
	}

	@Override
	public UserDTO create(UserDTO userDTO) {
		User user = dtoToEntity.convertUser(userDTO);
		entityManager.persist(user);
		entityManager.flush();
		userDTO.setId(user.getId());
		return userDTO;
	}

	@Override
	public UserDTO update(UserDTO userDTO) {
		User user = dtoToEntity.convertUser(userDTO);
		user.setId(userDTO.getId());
		user = entityManager.merge(user);
		return userDTO;
	}

	@Override
	public void delete(int id) {
		User user = entityManager.find(User.class, id);
		entityManager.remove(user);
	}

	@Override
	public UserDTO loginUser(LoginDTO loginDTO) throws CustomException {
		User user = null;

		try {
			user = entityManager.createNamedQuery("findUserByUsername", User.class)
					.setParameter("username", loginDTO.getUsername()).getSingleResult();
		} catch (NoResultException e) {
			throw new CustomException("Wrong authentication!");
		}
		if (!loginDTO.getPassword().equals(user.getPassword())) {
			throw new CustomException("Wrong authentication!");
		}

		UserDTO userDTO = entityToDTO.convertUser(user);
		return userDTO;

	}

	@Override
	public Boolean updatePassword(ChangePasswordDTO changePasswordDTO) throws CustomException {
		User user = null;

		try {
			user = entityManager.createNamedQuery("findUserByUsername", User.class)
					.setParameter("username", changePasswordDTO.getUsername()).getSingleResult();
			if (user.getPassword().equals(changePasswordDTO.getOldPassword())) {
				if (!changePasswordDTO.getOldPassword().equals(changePasswordDTO.getNewPassword())) {
					user.setPassword(changePasswordDTO.getNewPassword());
					user = entityManager.merge(user);
					return true;
				} else {
					throw new CustomException("Please choose another new password, not the same as the old one!");
				}
			} else
				throw new CustomException("The old password is not valid.");
		} catch (NoResultException e) {
			throw new CustomException("The username is not valid!");
		}
	}

	@Override
	public UserDTO registerUser(RegisterDTO registerDTO) throws CustomException {
		User user = null;

		try {
			
			TypedQuery<User> typedQuery = entityManager.createNamedQuery("registerUser", User.class);
			typedQuery
				.setParameter("username", registerDTO.getUsername())
				.setParameter("password", registerDTO.getPassword())
				.setParameter("email", registerDTO.getEmail());
			
			user = typedQuery.getSingleResult();
		} catch (NoResultException e) {
			throw new CustomException("Wrong authentication!");
		}

		UserDTO userDTO = entityToDTO.convertUser(user);
		return userDTO;
	}
}

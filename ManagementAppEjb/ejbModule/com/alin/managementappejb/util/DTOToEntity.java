package com.alin.managementappejb.util;

import com.alin.managementappejb.dto.IdentityDTO;
import com.alin.managementappejb.dto.OrganizationDTO;
import com.alin.managementappejb.dto.UserDTO;
import com.alin.managementappejb.model.Identity;
import com.alin.managementappejb.model.Organization;
import com.alin.managementappejb.model.User;

public class DTOToEntity {
	public User convertUser(UserDTO userDTO) {
		User user = new User();
		user.setName(userDTO.getUsername());
		user.setPassword(userDTO.getPassword());

		return user;
	}

	public Identity convertIdentity(IdentityDTO identityDTO) {
		return new Identity(identityDTO.getFirstName(), identityDTO.getLastName(), identityDTO.getOrganizationId());
	}

	public Organization convertOrganization(OrganizationDTO organizationDTO) {
		return new Organization(organizationDTO.getOrganizationName(), organizationDTO.getCui());
	}
}

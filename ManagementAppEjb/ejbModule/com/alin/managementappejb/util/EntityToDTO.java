package com.alin.managementappejb.util;

import com.alin.managementappejb.dto.IdentityDTO;
import com.alin.managementappejb.dto.OrganizationDTO;
import com.alin.managementappejb.dto.UserDTO;
import com.alin.managementappejb.model.Identity;
import com.alin.managementappejb.model.Organization;
import com.alin.managementappejb.model.User;

public class EntityToDTO {
	public UserDTO convertUser(User user) {
		UserDTO globalUserDTO = new UserDTO(user.getName(), user.getPassword());

		globalUserDTO.setId(user.getId());
		return globalUserDTO;
	}
	
	public IdentityDTO convertIdentity(Identity identity) {
		IdentityDTO globalIdentityDTO = new IdentityDTO(identity.getFirstName(), identity.getLastName(), identity.getOrganization().getId());

		globalIdentityDTO.setId(identity.getId());
		return globalIdentityDTO;
	}
	
	public OrganizationDTO convertOrganization(Organization organization) {
		OrganizationDTO orgDTO = new OrganizationDTO(organization.getName(), organization.getCui());

		orgDTO.setId(organization.getId());
		return orgDTO;

	}
}

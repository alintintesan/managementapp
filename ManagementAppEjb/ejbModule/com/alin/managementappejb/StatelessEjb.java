package com.alin.managementappejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.alin.managementappejb.model.User;

/**
 * Session Bean implementation class StatelessEjb
 */
@Stateless
public class StatelessEjb implements StatelessEjbRemote {
	
	@PersistenceContext
	private EntityManager entityManager;

    /**
     * Default constructor. 
     */
    public StatelessEjb() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void insert(String name) {
		User user = new User();
		user.setEmail("email");
		user.setPassword("password");
		user.setIdentityId(1);
		user.setName(name);
		entityManager.persist(new User());
	}
}

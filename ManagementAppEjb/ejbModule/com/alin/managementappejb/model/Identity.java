package com.alin.managementappejb.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the identity database table.
 * 
 */
@Entity
@NamedQuery(name="Identity.findAll", query="SELECT i FROM Identity i")
public class Identity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String firstName;

	private String lastName;

	//bi-directional many-to-one association to Organization
	@ManyToOne
	@JoinColumn(name="organizationId")
	private Organization organization;

	public Identity() {
	}
	
	public Identity(String firstName, String lastName, int organizationId) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		Organization o = new Organization();
		o.setId(organizationId);
		this.organization = o;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Organization getOrganization() {
		return this.organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

}
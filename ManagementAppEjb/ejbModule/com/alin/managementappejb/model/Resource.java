package com.alin.managementappejb.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the resource database table.
 * 
 */
@Entity
@NamedQuery(name="Resource.findAll", query="SELECT r FROM Resource r")
public class Resource implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String accessKey;

	private String token;

	private String url;

	//bi-directional many-to-one association to Userroleresource
	@OneToMany(mappedBy="resource")
	private List<Userroleresource> userroleresources;

	public Resource() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccessKey() {
		return this.accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public String getToken() {
		return this.token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<Userroleresource> getUserroleresources() {
		return this.userroleresources;
	}

	public void setUserroleresources(List<Userroleresource> userroleresources) {
		this.userroleresources = userroleresources;
	}

	public Userroleresource addUserroleresource(Userroleresource userroleresource) {
		getUserroleresources().add(userroleresource);
		userroleresource.setResource(this);

		return userroleresource;
	}

	public Userroleresource removeUserroleresource(Userroleresource userroleresource) {
		getUserroleresources().remove(userroleresource);
		userroleresource.setResource(null);

		return userroleresource;
	}

}
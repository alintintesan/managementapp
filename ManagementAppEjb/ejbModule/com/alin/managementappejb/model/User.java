package com.alin.managementappejb.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
@NamedQuery(name = "findUserByUsername", query = "SELECT u FROM User u WHERE u.name = :username")
//@NamedQuery(name = "registerUser", query = "INSERT INTO User('name', 'email', password') VALUES(:username, :email, :password)")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String email;

	private int identityId;

	private String name;

	private String password;

	//bi-directional many-to-one association to Userroleresource
	@OneToMany(mappedBy="user")
	private List<Userroleresource> userroleresources;

	public User() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getIdentityId() {
		return this.identityId;
	}

	public void setIdentityId(int identityId) {
		this.identityId = identityId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Userroleresource> getUserroleresources() {
		return this.userroleresources;
	}

	public void setUserroleresources(List<Userroleresource> userroleresources) {
		this.userroleresources = userroleresources;
	}

	public Userroleresource addUserroleresource(Userroleresource userroleresource) {
		getUserroleresources().add(userroleresource);
		userroleresource.setUser(this);

		return userroleresource;
	}

	public Userroleresource removeUserroleresource(Userroleresource userroleresource) {
		getUserroleresources().remove(userroleresource);
		userroleresource.setUser(null);

		return userroleresource;
	}
}
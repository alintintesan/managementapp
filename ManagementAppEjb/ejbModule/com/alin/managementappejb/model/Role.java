package com.alin.managementappejb.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the role database table.
 * 
 */
@Entity
@NamedQuery(name="Role.findAll", query="SELECT r FROM Role r")
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String description;

	private String name;

	//bi-directional many-to-many association to Right
	@ManyToMany(mappedBy="roles")
	private List<Right> rights;

	//bi-directional many-to-one association to Userroleresource
	@OneToMany(mappedBy="role")
	private List<Userroleresource> userroleresources;

	public Role() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Right> getRights() {
		return this.rights;
	}

	public void setRights(List<Right> rights) {
		this.rights = rights;
	}

	public List<Userroleresource> getUserroleresources() {
		return this.userroleresources;
	}

	public void setUserroleresources(List<Userroleresource> userroleresources) {
		this.userroleresources = userroleresources;
	}

	public Userroleresource addUserroleresource(Userroleresource userroleresource) {
		getUserroleresources().add(userroleresource);
		userroleresource.setRole(this);

		return userroleresource;
	}

	public Userroleresource removeUserroleresource(Userroleresource userroleresource) {
		getUserroleresources().remove(userroleresource);
		userroleresource.setRole(null);

		return userroleresource;
	}

}
package standaloneClient;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.alin.managementappejb.StatelessEjbRemote;

public class Main {

	public static void main(String[] args) throws NamingException {
		InitialContext context = new InitialContext();
		StatelessEjbRemote ejb = (StatelessEjbRemote) context
				.lookup("java:global/ManagementAppEjbEAR/ManagementAppEjb/StatelessEjb!com.alin.managementappejb.StatelessEjbRemote");
		ejb.insert("Alin Tintesan test");
	}
}

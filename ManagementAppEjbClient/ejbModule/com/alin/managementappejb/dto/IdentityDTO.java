package com.alin.managementappejb.dto;

import java.io.Serializable;

public class IdentityDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3816194704372954590L;

	private int id;
	private String firstName;
	private String lastName;
	private int organizationId;
	
	public IdentityDTO() {
		
	}

	public IdentityDTO(String firstName, String lastName, int organizationId) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.organizationId = organizationId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(int organizationId) {
		this.organizationId = organizationId;
	}

	@Override
	public String toString() {
		return "IdentityDTO [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", organizationId="
				+ organizationId + "]";
	}
}

package com.alin.managementappejb.dto;

import java.io.Serializable;

public class OrganizationDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4988267838477309279L;

	private int id;
	private String organizationName;
	private String cui;
	
	public OrganizationDTO() {
		super();
	}

	public OrganizationDTO(String organizationName, String cui) {
		super();
		this.organizationName = organizationName;
		this.cui = cui;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getCui() {
		return cui;
	}

	public void setCui(String cui) {
		this.cui = cui;
	}

	@Override
	public String toString() {
		return "OrganizationDTO [id=" + id + ", organizationName=" + organizationName + ", cui=" + cui + "]";
	}
}

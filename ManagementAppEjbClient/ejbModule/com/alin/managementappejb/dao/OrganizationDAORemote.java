package com.alin.managementappejb.dao;

import java.util.List;

import javax.ejb.Remote;

import com.alin.managementappejb.dto.OrganizationDTO;
import com.alin.managementappejb.exception.CustomException;

@Remote
public interface OrganizationDAORemote extends GenericDAO<OrganizationDTO> {

	OrganizationDTO addOrganization(OrganizationDTO organizationDTO) throws CustomException;
	List<OrganizationDTO> getOrganizations() throws CustomException;
}

package com.alin.managementappejb.dao;

import javax.ejb.Remote;

import com.alin.managementappejb.dto.ChangePasswordDTO;
import com.alin.managementappejb.dto.LoginDTO;
import com.alin.managementappejb.dto.RegisterDTO;
import com.alin.managementappejb.dto.UserDTO;
import com.alin.managementappejb.exception.CustomException;

@Remote
public interface UserDAORemote extends GenericDAO<UserDTO> {
	UserDTO loginUser(LoginDTO loginDTO) throws CustomException;
	Boolean updatePassword(ChangePasswordDTO changePasswordDTO) throws CustomException;
	UserDTO registerUser(RegisterDTO registerDTO) throws CustomException;
}

package com.alin.managementappejb.dao;

import javax.ejb.Remote;

import com.alin.managementappejb.dto.IdentityDTO;
import com.alin.managementappejb.dto.OrganizationDTO;
import com.alin.managementappejb.exception.CustomException;

@Remote
public interface IdentityDAORemote extends GenericDAO<IdentityDTO> {
	
	IdentityDTO addIdentity(IdentityDTO identityDTO) throws CustomException;
	boolean registerIdentity(IdentityDTO identityDTO) throws CustomException;
}

package com.alin.managementappejb.exception;

import javax.ejb.EJBException;

public class CustomException extends EJBException {

	private static final long serialVersionUID = -1810536446379742707L;
	
	private String message;

	public CustomException(String message) {
		super(message);
		this.message = message;
	}

	public String message() {
		return this.message;
	}
}

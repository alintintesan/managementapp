package com.alin.managementappejb;

import javax.ejb.Remote;

@Remote
public interface StatelessEjbRemote {

	void insert(String name);
}

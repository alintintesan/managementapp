package managedBean;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.alin.managementappejb.dao.OrganizationDAORemote;
import com.alin.managementappejb.dto.OrganizationDTO;

@ManagedBean
@SessionScoped
public class OrganizationBean {

	OrganizationDTO organizationDTO = new OrganizationDTO();
	private List<OrganizationDTO> organizations = new ArrayList<>();
	
	public List<OrganizationDTO> getOrganizations() {
		if (this.organizations.size() == 0) {
			this.organizations = this.organizationDAORemote.findAll();
		}
		
		return organizations;
	}

	public void setOrganizations(List<OrganizationDTO> organizations) {
		this.organizations = organizations;
	}

	@EJB
	OrganizationDAORemote organizationDAORemote;

	public OrganizationDTO getOrganizationDTO() {
		return organizationDTO;
	}

	public void setOrganizationDTO(OrganizationDTO organizationDTO) {
		this.organizationDTO = organizationDTO;
	}
	
	public String addNewOrganization() {
		this.organizationDAORemote.addOrganization(this.organizationDTO);
		return "/adminFilter/admin.xthml?faces-redirect=true";
	}
	
	public String goToNewOrganizationPage() {
		return "/organization.xhtml?faces-redirect=true";
	}
	
	public String logout() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/index?faces-redirect=true";
	}
}

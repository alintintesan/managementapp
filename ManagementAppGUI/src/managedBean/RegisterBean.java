package managedBean;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.alin.managementappejb.dao.UserDAORemote;
import com.alin.managementappejb.dto.RegisterDTO;
import com.alin.managementappejb.dto.UserDTO;
import com.alin.managementappejb.exception.CustomException;

@ManagedBean
@SessionScoped
public class RegisterBean {

	RegisterDTO registerDTO = new RegisterDTO();

	@EJB
	UserDAORemote userDAORemote;

	UserDTO userDTO;

	public RegisterDTO getRegisterDTO() {
		return registerDTO;
	}

	public void setRegisterDTO(RegisterDTO registerDTO) {
		this.registerDTO = registerDTO;
	}

	public UserDTO getUserDTO() {
		return userDTO;
	}

	public void setUserDTO(UserDTO userDTO) {
		this.userDTO = userDTO;
	}

	public String registerUser() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		try {
			userDTO = userDAORemote.registerUser(registerDTO);
			facesContext.getExternalContext().getSessionMap().put("userDTO", userDTO);
			System.out.println(userDTO.getUsername() + " registered");
			return "/userFilter/user.xhtml?faces-redirect=true";
		} catch (CustomException e) {
			System.out.println("Email, username or password already exist!");
			facesContext.addMessage("registerForm", new FacesMessage(FacesMessage.SEVERITY_ERROR, e.message(), null));
			return null;
		}
	}
}

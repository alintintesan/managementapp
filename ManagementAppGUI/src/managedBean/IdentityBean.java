package managedBean;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.alin.managementappejb.dao.IdentityDAORemote;
import com.alin.managementappejb.dto.IdentityDTO;

@ManagedBean
@SessionScoped
public class IdentityBean {

	private IdentityDTO identityDTO = new IdentityDTO();
	private List<IdentityDTO> identities = new ArrayList<>();
	
	@EJB
	IdentityDAORemote identityDAORemote;
	
	public IdentityDTO getIdentityDTO() {
		return identityDTO;
	}
	
	public void setIdentityDTO(IdentityDTO identityDTO) {
		this.identityDTO = identityDTO;
	}
	
	public List<IdentityDTO> getIdentities() {
		if (this.identities.size() == 0) {
			this.identities = this.identityDAORemote.findAll();
		}
		
		return identities;
	}
	
	public void setIdentities(List<IdentityDTO> identities) {
		this.identities = identities;
	}
	
	public String addNewIdentity() {
		this.identityDAORemote.addIdentity(this.identityDTO);
		return "/adminFilter/admin.xthml?faces-redirect=true";
	}
	
	public String goToNewIdentityPage() {
		return "/identity.xhtml?faces-redirect=true";
	}
}


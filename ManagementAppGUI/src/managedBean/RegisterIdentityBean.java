package managedBean;

import java.util.List;
import java.util.ArrayList;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.alin.managementappejb.dto.OrganizationDTO;
import com.alin.managementappejb.exception.CustomException;
import com.alin.managementappejb.dao.IdentityDAORemote;
import com.alin.managementappejb.dao.OrganizationDAORemote;
import com.alin.managementappejb.dto.IdentityDTO;

@ManagedBean
@SessionScoped
public class RegisterIdentityBean {
	
	IdentityDTO identityDTO = new IdentityDTO();

	public IdentityDTO getIdentityDTO() {
		return identityDTO;
	}

	public void setIdentityDTO(IdentityDTO identityDTO) {
		this.identityDTO = identityDTO;
	}
	
	@EJB
	IdentityDAORemote identityDAORemote;
	
	@EJB
	OrganizationDAORemote organizationDAORemote;
	
	private List<OrganizationDTO> organizations = new ArrayList<>();

	public List<OrganizationDTO> getOrganizations() {
		if (this.organizations.size() == 0) {
			this.organizations = organizationDAORemote.findAll();
		}
		
		return organizations;
	}

	public void setOrganizations(List<OrganizationDTO> organizations) {
		this.organizations = organizations;
	}
	
	public String registerIdentity() {
		try {
			identityDAORemote.registerIdentity(identityDTO);
			return "/index.xhtml?faces-redirect=true";
		} catch (CustomException e) {
			System.out.println("Wrong identity data.");
		}
		
		return "";
	}
}
